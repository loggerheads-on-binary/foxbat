from argparse import ArgumentParser, Namespace 
from annautils.getpass import getpass
import os  
import logging
import shutil 
from .__init__ import *
import warnings

def arguments(args : list = None ):

    global FOXBAT_DEFAULT_REDUNDANCY, FOXBAT_REMOVE_SOURCE_FILES

    parser = ArgumentParser(prog = "foxbat" , description = "Encryption script for small folders")

    security_parsers = parser.add_argument_group("Types of security")

    security_parsers.add_argument('--foxbat' , '--non-redundant' , action = 'store_const' , dest = 'redundancy' , default = FOXBAT_DEFAULT_REDUNDANCY, const = False, 
                        help = 'Encode/decode using the extra safe FOXBAT method. Can also be set using the environment variable FOXBAT=1')

    
    security_parsers.add_argument('--foxhound' , '--redundant' , action = 'store_const' , dest = 'redundancy' , default = FOXBAT_DEFAULT_REDUNDANCY, const = True, 
                        help = 'Encode/decode using the more accessible but less secure FOXHOUND method. Can also be set using the environment variable FOXBAT=0 or FOXHOUND=1'  )

    mode_parsers = parser.add_argument_group("Modes of operation")

    mode_parsers.add_argument('-e' , '--encrypt' , default = None , action = 'store_const' , const = 'encrypt' , dest = 'mode' , 
                        help = 'Encrypt the source directory')

    mode_parsers.add_argument('-d' , '--decrypt' ,default = None , action = 'store_const' , const = 'decrypt' , dest = 'mode' , 
                        help = 'Decrypt the encrypted archive' )

    parser.add_argument('-o' , '--output' , type = os.path.abspath , default = None , 
                        help = "Output file path")
                        
    parser.add_argument('file' , type = os.path.abspath , help = 'Input file')

    parser.add_argument('-p' , '--password' , default = None, 
                        help = 'Password for encryption/decryption')

    parser.add_argument('--clear' , action = 'store_true' , help = 'Give a clear input password')
    parser.add_argument('--rm' , '--remove-source-files' , '--remove-source-file' , '--remove-source-dir' , 
                        dest  = 'file_del' , action = 'store_true' , default = FOXBAT_REMOVE_SOURCE_FILES , 
                        help = "Remove the source file/directory. Can also be set by creating an environment variable FOXBAT_RM_SRC")

    if args is None:
        return parser.parse_args()  
    
    return parser.parse_args(args)

def driver(args: Namespace):

    global logger 

    args.redundancy = get_redundancy(args.redundancy)
    set_buffer(args.buffer_size)
    set_buffer_file(args.in_memory_buffer)
    
    create_logger('foxhound' if args.redundancy else 'foxbat')

    args.mode = find_type(args.file , args.mode)
    logger.debug(f"Mode: {args.mode}")

    args.output = make_output(args.output , args.mode , args.file , args.redundancy)
    logger.debug(f"Output File: {args.output}")

    if args.password is None:
        args.password = getpass(  f"Enter {args.mode}ion password" , colored = True , mask = (args.clear == False))

    if args.mode == 'encrypt':

        if not os.path.isdir(args.file):
            warnings.warn( f'Source is not a directory' , Warning)

        logger.info(f"Setting up encryption for {args.file}")
        encrypt(args.file ,args.output , args.redundancy , args.password)

    if args.mode == 'decrypt':

        assert not os.path.isfile(args.output), f'Output path already exists as a file'

        logger.info(f"Setting up decryption for {args.file}")
        decrypt(args.file ,args.output , args.redundancy , args.password)

    if args.file_del:
        
        if args.mode == 'encrypt':
            
            logger.critical("Deleting source directory")
            
            if os.path.isdir(args.file):
                shutil.rmtree(args.file)

            elif os.path.isfile(args.file):
                os.remove(args.file)
                
            logger.warning("Source directory has been completely deleted")

        else: 

            logger.critical("Deleting source archive file")
            os.remove(args.file)
            logger.warning("Source archive file has been removed")

if __name__ == '__main__': 

    import pretty_traceback , coloredlogs

    coloredlogs.install(fmt = "[%(name)s] %(asctime)s %(levelname)s : %(message)s" , level = logging.DEBUG)
    pretty_traceback.install() 

    args = arguments()

    logger = logging.getLogger("main")
    logger.setLevel(logging.DEBUG)

    logger.debug("Arguments parsed")
    driver(args)